Facts
-----

Module for creation of strucured external facts in yaml format.

Facts class looks up 'facts::list' entries in hiera and creates facts::fact
resources putting fact files into /etc/puppetlabs/facter/facts.d directory.
One file can contain multiple facts. 

Contents of the 'data' has is dumped into a yaml facts file as-is. It
allows for creation of the structured external facts of any complexity.

## Examples

In the manifest:

```
facts::fact { 'fact_file_name':
  ensure => 'present', 
  data => {
    'key1' => 'value1',
    'key2' => 'value2' 
  } 
}
```

In hiera the fact definition would look like

```
facts::list:
  fact_file_name:
    ensure: present
    data:
      fact1: 'value1'
      fact2: 'value2'
      fact3:
        - 'value3'
        - 'value4'
  'fact_file_name2':
    'ensure': absent
```
