# Return the network configuration of the 
# out-of-band controller for physical servers.
#
Facter.add(:bmc) do
  confine :kernel => 'Linux'
  confine :is_virtual => false
  confine do
    File.exist?('/usr/bin/ipmitool')
  end

  def parse_lan(reply)
    parsed = {}
    key = ''
    reply.each_line do |line|
      line_array = line.split(':')
      if line_array[0].strip.empty?
        original_value = parsed[key]
        if line_array.count == 3 && !original_value.is_a?(Hash)
          subline_array = original_value.split(':')
          subkey = subline_array.slice!(0).strip
          subvalue = subline_array.join(':').strip
          parsed.delete(key)
          parsed[key] = Hash[subkey, subvalue]
        elsif line_array.count == 3
          subkey = line_array.slice!(1).strip
          subvalue = line_array.join(':').strip
          parsed[key][subkey] = subvalue
        end
      else
        key = line_array.slice!(0).strip
        value = line_array.join(':').strip
        if key == 'IP Address Source'
          case value
          when %r{static}i
            value = 'static'
          when %r{dhcp}i
            value = 'dhcp'
          when %r{none}i
            value = 'none'
          when %r{bios}i
            value = 'bios'
          end
        end
        parsed[key] = value
      end
    end
    parsed
  end

  setcode do
    begin
      ipmitool_output = parse_lan(Facter::Util::Resolution.exec("/usr/bin/ipmitool lan print 1"))
      {
        :ip_source => ipmitool_output['IP Address Source'],
        :ip => ipmitool_output['IP Address'],
        :mac => ipmitool_output['MAC Address'],
        :gateway => ipmitool_output['Default Gateway IP'],
        :netmask => ipmitool_output['Subnet Mask'],
      }
    rescue NoMethodError
      nil
    end
  end
end
