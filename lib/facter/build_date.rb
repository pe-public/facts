# Determine build date of the server based on the
# date of the creation of the root file system.
#
require "date"

Facter.add(:build_date) do
  setcode do
    begin
      os_family = Facter.value(:osfamily)
      if os_family == 'RedHat'
        Date.parse(Facter::Util::Resolution.exec("rpm -q basesystem --qf '%{INSTALLTIME:day}'")).to_s
      else
        root_vol = Facter.value(:mountpoints)['/']['device']
        Date.parse(/^Filesystem created:\s+(.*)$/.match(%x{tune2fs -l #{root_vol} 2>&1})[1]).to_s
      end
    rescue NoMethodError
      nil
    end
  end
end
