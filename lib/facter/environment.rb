# Fact: environment
#
# Purpose: Returns the Puppet environmet (e.g., 'idg_master', 'production',
# etc).
#
# Note: When run as 'facter -p' this always returns 'production'.

require 'puppet'

Facter.add(:environment) do
  setcode do
    Puppet[:environment]
  end
end
