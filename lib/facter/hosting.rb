Facter.add(:hosting) do
    setcode do
        confine :kernel => :linux

        def product(file_name)
            file_path = "/sys/class/dmi/id/#{file_name}"
            if File.file?(file_path)
                return File.open(file_path, &:readline)
            end
        end

        p = Hash.new

        p["aws"] = product("bios_version").match(/amazon/i)
        p["gcp"] = product("product_name").match(/Google/)
        p["azure"] = product("sys_vendor").match(/Microsoft Corporation/)
        p["stanford"] = !(p["aws"] || p["gcp"] || p["azure"])

        cloud = "stanford"
        p.each do |k,v|
            cloud = k if p[k]
            p[k] = p[k] ? 'present':'absent'
        end  

        p["cloud"] = cloud
        p
    end
end
