# 'Approximates' the last date that the system was patched on.
# Since this can be a bit tricky to determine, given that some
# packages such as the datadog agent auto-update, this routine
# declares that linux kernel patching qualifies as a total system
# patch. This should fly, given our 90-day patching cycle
#
# In the event that it doesn't, some catch-all logic in the Debian
# family of OS added should take care of it with reasonable certainty
#
# Example:
# 2017-01-31

Facter.add(:last_patched) do
  setcode do
    begin
      family = Facter.value('osfamily')
      case family
      when "RedHat"
        result = Facter::Util::Resolution.exec('rpm -q kernel --last | head -n1 | tr -s " "  | cut -d " " -f2- | xargs -0 date +"%Y-%m-%d" -d')       
      when "Debian"
        result = Facter::Util::Resolution.exec('zgrep -h install /var/log/dpkg.log* | grep linux-image | grep -v half | sort | tail -n1 | cut -d "s" -f1 | cut -d " " -f1')
        # new logic: uptime equaling date when sufficient number of packages applied
        uptime_date = Facter::Util::Resolution.exec('echo $(expr `date +%s` - `cat /proc/uptime | cut -d "." -f 1` ) | xargs -i date +%Y-%m-%d -d @{}')
        uptime_pkgs = Facter::Util::Resolution.exec("grep #{uptime_date} /var/log/dpkg.log | wc -l")
        if ((uptime_pkgs.to_i > 250) && (uptime_date > result))
          result = uptime_date
        end
      else
        result = "unknown"
      end
    rescue => e
      result = "unknown"
    end
    # check for empty result
    if ((result.nil?) || (result.length <= 1))
      "unknown"
    else
      result
    end 
  end
end


# Perform a days elapsed calculation between today and when
# we believe the system was last patched (:last_patched)
# If :last_patched is not defined or malformed, result will be “unknown”
#
# Example:
# 89
 
require 'date'
 
Facter.add(:last_patched_days) do
  setcode do
    begin
      # This uses Modified Julian Day Number (mjd) for calculation
      result = Date.today.mjd - Date.parse(Facter.value(:last_patched)).mjd
    # trap any exceptions (likely ArgumentError (malformed) or TypeErrors (nil))
    rescue => e
      "unknown"
    end
  end
end
