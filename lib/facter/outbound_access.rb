# Fact determines if a host has outbound access to the internet.
# It port scans a reliable web host (google.com as of now) on
# port 443. Returns 'true' if it gets a response, otherwise 'false'.

Facter.add(:wan) do
  setcode do
	require 'socket'
	require 'resolv'

	TIMEOUT = 0.5

	def scan_port(port, host)
	  socket      = Socket.new(:INET, :STREAM)
	  remote_addr = Socket.sockaddr_in(port, host)

	  begin
		socket.connect_nonblock(remote_addr)
	  rescue Errno::EINPROGRESS
	  end

	  _, sockets, _ = IO.select(nil, [socket], nil, TIMEOUT)

	  sockets.nil? ? 'false':'true'
	end

	scan_port(443, Resolv.getaddress('google.com'))
  end
end
