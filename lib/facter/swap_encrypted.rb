# swap_encrypted: 
#	Report whether the OS swap volume is encrypted ( true | false )
#   Assumes: encryption done with cryptsetup
#	Known Issues: 
#		- Limitation of assumption (e.g. encrypted LVM not detected)
#		- Only reliable on systems with a single swap partition defined
#
Facter.add(:swap_encrypted) do
  setcode do
    result = 'false'
    swap_loc = Facter::Util::Resolution.exec('/sbin/swapon -s | /usr/bin/tail -n 1 | /usr/bin/cut -f 1 -d \' \'') 
    crypt_status = Facter::Util::Resolution.exec("/sbin/cryptsetup status #{swap_loc} 2>/dev/null")
    if (crypt_status != nil)
      if crypt_status.include? "cipher"
        result = 'true'
      end
    end
    result
  end
end
