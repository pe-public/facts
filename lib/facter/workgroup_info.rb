# This fact queries Stanford ldap for the information about POSIX groups
# typically derived from a Stanford Workgroups. The information returned
# is a hash of workgroups with or without their user membership.
# 
# A scope of the query depends on a separate external fact "workgroups",
# which must have the following structure:
#
#        workgroups => {
#        	list => [
#        		"pattern1",
#        		"pattern2",
#        		...
#        	],
#        	membership => true,
#        	keytab => "/path/to/ldap_access.keytab",
#        	principal => "service/principal"
#        }
#
# "list" array contains a list of patterns in workgroup names. Typically it
# is either explicit list of workgroups or '*' denoting all workgroups
# accessible to the "principal" in LDAP. 
#
# "principal" is optional. If it is omited, the fact uses the first principal
# found in a file specified in the "keytab" key.
#
# If "membership" key is set to "true", the fact returns a hash of workgroup
# names as keys and an array of members as a value. Otherwise an array of
# members comes back as empty.
#
# An example of the output with membership:
#
# 			workgroup_info => {
# 				mpcore:srv-www-hemestamp => [
# 					"atayts",
# 					"diwashj",
# 					"stehr",
# 					"k3nn3dy",
# 					"fionay"
# 				],
# 				mpcore:srv-vbox-users => [
# 					"atayts",
# 					"diwashj",
# 					"stehr",
# 					"fionay",
# 					"jtung1"
# 				]
# 			}
#
# and without:
#
#       workgroup_info => {
#         mpcore:srv-www-hemestamp => [],
#         mpcore:srv-vbox-users => []
#       }
#

Facter.add(:workgroup_info) do
    confine :kernel => :linux
    confine :workgroups do |value|
      !value.nil? and !value['keytab'].nil?
    end

    setcode do
        wghash = Facter.value(:workgroups)
        attribute = wghash['membership'] ? 'memberUid' : 'cn'
        workgroups = {}

        wghash['list'].each do |pattern|

          # if principal is specified, use it rather than the first
          # found in the keytab
          if wghash.has_key?('principal')
            kstart_opts = "-qf #{wghash['keytab']} #{wghash['principal']}"
          else
            kstart_opts = "-Uqf #{wghash['keytab']}"
          end

          ldap_search = "/usr/bin/k5start " + kstart_opts + " -- ldapsearch -Y GSSAPI -Q -LLL -o ldif-wrap=no -h ldap.stanford.edu -b cn=groups,dc=stanford,dc=edu \"(&(objectClass=suPosixGroup)(cn=#{pattern}))\" #{attribute}"

          ldap_out = Facter::Core::Execution.execute(ldap_search)
          if !ldap_out.empty?

            # multiple workgroups may be returned
            wgs = ldap_out.split("\n\n")
            wgs.each do |wg|
                membership = wg.split("\n").map {|n| n.split(': ')[1]}
                workgroup = membership.shift().match(/^cn=([^,]+)/)[1]

                # if querying just workgroup names, set membership to empty
                workgroups[workgroup] = wghash['membership'] ? membership : []
            end
          end

        end

        workgroups.empty? ? nil : workgroups
    end
end

