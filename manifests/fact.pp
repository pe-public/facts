# Provides arbitrary named facts

define facts::fact (
  Enum['absent', 'present'] $ensure = present,
  Hash $data,
) {
  # filename of a fact
  $fact_file = "/etc/puppetlabs/facter/facts.d/${name}.yaml"

  file { $fact_file:
    ensure  => $ensure,
    content => to_yaml($data),
  }
}
