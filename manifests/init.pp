# Module for creation structured facts in yaml format.
#
# Facts class looks up 'facts::list' entries in hiera and creates facts::fact
# resources putting fact files into /etc/puppetlabs/facter/facts.d directory.
# One file can contain multiple facts. 
# 
# Contents of the 'data' has is dumped into a yaml facts file as-is. It
# allows for creation of the structured external facts of any complexity.
#
# Usage
#   class { 'facts':
#     list => { 
#       ensure => 'present', 
#       data => {
#         'key1' => 'value1',
#         'key2' => 'value2' 
#       } 
#     },
#   }
#
# In hiera the fact definition would look like
#   facts::list:
#     fact_file_name:
#       ensure: present
#       data:
#         fact1: 'value1'
#         fact2: 'value2'
#         fact3:
#           - 'value3'
#           - 'value4'
#     'fact_file_name2':
#       'ensure': absent
#
# If you need to define multiple facts within manifests, use facts::fact
# resource:
#   facts::fact {'fact_file_name':
#     ensure    => present,
#     data => { 'key1' => 'value1', 'key2' => 'value2' },
#   }
#

class facts (
  $list = {}
) {
  # ensure fact directory exits since 
  # it is not created by default.
  file { [
    '/etc/puppetlabs/facter',
    '/etc/puppetlabs/facter/facts.d'
  ]:
    ensure  => directory,
    recurse => true,
    force   => true,
    purge   => true;
  }

  # find all fact definitions in hiera
  create_resources('facts::fact', $list)
}
